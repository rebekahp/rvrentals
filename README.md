# RVrentals - project to try some web scraping

## Purpose

This project gave me a reason to try web scraping on a practical problem - with coronavirus, we wanted to travel, but being able to make food and eat on our own, and having our own bathroom, would be really helpful. We discovered two websites that have peer to peer RV rentals - RVshare and outdoorsy. I used beautifulsoup on the rvshare site and selenium on outdoorsy. This let us quickly search all of the vehicles for rent in our area for the characteristics we wanted. By looking at the spreadsheet, we can quickly narrow down what we want, pick a model and price that fits, check its rough availability, and then go request a reservation for the one we'd ilke to use!

## How to use
This has not been made friendly to other users yet. You'll need to visit the actual websites, put in the search filters you want, and then use that as your initial URL for the scripts. The class names of the page elements also change from time to time and I haven't figured out a good way to handle that within the scripts.