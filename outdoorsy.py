import time
import pandas as pd
from selenium import webdriver

driver = webdriver.Chrome('/Users/reo327/Downloads/chromedriver 2')

driver.get('https://www.outdoorsy.com/rv-search?address=Austin%2C%20TX%2C%20USA&filter%5Btype%5D=c%2Ccamper-van%2Cb%2Ca%2Ctruck-camper%2Cother&map=false&sleeps%5Badults%5D=2')
links = driver.find_elements_by_css_selector('[class^=_Title]')
features = driver.find_elements_by_css_selector('[class^=_feature]')
# links = driver.find_elements_by_class_name('_Title_1rpq6n')
# features = driver.find_elements_by_class_name('_feature_gzjlcu')

idx = 1
shorterRVs = []

# This is gross-feeling, by the "Length" is the second one of each, so it works.
for feature in features:
    if 'Length' in feature.text and feature.text[-4:-2] < '26':
        shorterRVs.append(int(idx/2-1))
    idx+=1

rvsToCheck = []
for num in shorterRVs:
    rvsToCheck.append(links[num].get_attribute("href"))

toCsv = []

for rv in rvsToCheck:
    keyDetails = {}
    time.sleep(5)
    driver.get(rv)
    amenities = driver.find_elements_by_class_name('item_1fcp8')
    amText = [am.text for am in amenities]
    if 'Toilet' in amText:
        keyDetails['toilet'] = True
    mainDetails = driver.find_elements_by_class_name('_Text_kux1gm')
    detailText = [detail.text for detail in mainDetails]
    keyDetails['sleeps']=detailText[0].split(" ")[1]
    keyDetails['length']=detailText[1].split(" ")[1]
    keyDetails['rv_year']=detailText[2].split(" ")[1]
    keyDetails['make_model']=detailText[4]

    rates = driver.find_elements_by_class_name('block_cz68n')
    ratesText = [rate.text for rate in rates]
    keyDetails['Nightly rate']=ratesText[3].split("\n")[1]
    keyDetails['Weekly rate']=ratesText[4].split("\n")[1]
    keyDetails['Monthly rate']=ratesText[5].split("\n")[1]

    booked = driver.find_elements_by_class_name('booked')
    bookedText = [day.text for day in booked]
    bookedText = [date for date in bookedText if date != '']
    keyDetails['Days Booked']=len(bookedText)
    keyDetails['link']=rv

    toCsv.append(keyDetails)

towrite = pd.DataFrame(toCsv)
towrite.to_csv('OutRVcomps.csv')
