import requests
import urllib.request
import time
from bs4 import BeautifulSoup
import csv
import re
import json
import pandas as pd
from baseRv import baseRv
from RvShareRV import RvShareRV

initialUrl = 'https://rvshare.com/rv-rental?location=78705&lat=30.2961708&lng=-97.73895429999999&max_length=25&rv_class=Class%20A%20Motor%20Home%2CClass%20B%20Camping%20Van%2CClass%20C%20Motor%20Home&sleeps=2'
response = requests.get(initialUrl)

soup = BeautifulSoup(response.text, "html.parser")
rvlist = soup.findAll('a')[2:] #first RV is the third a-tag
rvlinks = set()

baseURL = 'https://rvshare.com'

rvsToKeep = {}

# There's a good chance that these aren't all rvs, since we just grabbed all the a-tags.
for rv in rvlist:
    if re.search('\/rvs\/details\/', rv['href']):
        rvlinks.add(rv['href'])

print(rvlinks)

toCsv= []

for link in rvlinks:
    keyDetails = {}
    time.sleep(5)
    rv.detail_url = baseURL + link
    keyDetails['url']=link
    details = requests.get(rv.detail_url)
    detailsoup = BeautifulSoup(details.text, "html.parser")

    # this is the text that shows up from the main search page and on the rv page
    try:
        rv = RvShareRV(detailsoup.find(class_="RvTitle__Headline-hskm81-2").text)
    except:
        break

    summaryDiv = detailsoup.find_all(class_="styles__SummaryItem-sc-1ylo37q-1")
    if len(summaryDiv)>0:
        rv.location = summaryDiv[0].text
        keyDetails['location']=rv.location

    amenities = detailsoup.findAll(class_='amenity-item')
    namedAmenities = {}
    rv.other_amenities = []
    for item in amenities:
        if re.search('\-', item.text):
            namedAmenities[item.text.split(" - ")[0]] = item.text.split(" - ")[1]
        else:
            rv.other_amenities.append(item.text)

    keyDetails['toilet']= 'Toilet' in rv.other_amenities
    keyDetails['shower'] = 'Shower' in rv.other_amenities
    keyDetails['rear cam']= 'Rear Vision Camera' in rv.other_amenities

    # fill up from that dictionary! some of these are wanted for printing, too
    keyDetails['manufacturer']=rv.manufacturer = namedAmenities.get('Manufacturer')
    keyDetails['make']=rv.make = namedAmenities.get('Make')
    keyDetails['model']=rv.model = namedAmenities.get('Model')
    keyDetails['rv_class']=rv.rv_class = namedAmenities.get('Class')
    keyDetails['sleeps']=rv.sleeps = namedAmenities.get('Sleeps')
    keyDetails['rv_year']=rv.rv_year = namedAmenities.get('Year')
    rv.num_beds = namedAmenities.get('Total Number of Beds')
    rv.fresh_water_tank = namedAmenities.get('Fresh water tank')
    keyDetails['length']=rv.length = namedAmenities.get('Length')
    rv.height = namedAmenities.get('Height')
    rv.transmission = namedAmenities.get('Transmission')
    rv.cruise_control = namedAmenities.get('Cruise control')
    rv.engine = namedAmenities.get('Engine')
    keyDetails['seatbelts']=rv.seatbelts = namedAmenities.get('Seatbelts')
    keyDetails['fuel type']=rv.fuel_type = namedAmenities.get('Fuel type')
    rv.fuel_capacity = namedAmenities.get('Fuel capacity')
    keyDetails['mpg']=rv.fuel_mpg = namedAmenities.get('Fuel consumption')
    rv.electric_service = namedAmenities.get('Electrical service')
    rv.dual_battery = namedAmenities.get('Dual battery')
    rv.power_steering = namedAmenities.get('Power steering')
    rv.gross_weight = namedAmenities.get('Gross weight')
    rv.dry_weight = namedAmenities.get('Dry weight')
    rv.cargo_weight = namedAmenities.get('Cargo weight')

    times = detailsoup.findAll(class_="center py2 col col-6 time")
    try:
        rv.pick_up_time = times[0].find(class_='box-header').text
    except:
        pass
    try:
        rv.drop_off_time = times[1].find(class_='box-header').text
    except:
        pass

    rules = detailsoup.findAll(class_='flex items-center justify-center box rentalrules')
    rv.rules = [rule.text for rule in rules]

    rates = detailsoup.findAll(class_='RateBoxes__DesktopWrapper-rrs11v-1')
    rv.rates={}
    for rate in rates:
        rv.rates[rate.find(class_='box-footer').text] = rate.find(class_='box-header').text

    keyDetails['Nightly rate'] = {rv.rates.get('NIGHTLY')}
    keyDetails['Weekly rate'] = {rv.rates.get('WEEKLY')}
    keyDetails['Monthly rate'] = {rv.rates.get('MONTHLY')}

    fees = detailsoup.findAll(class_='Fees__Fee-tkzxnr-2')
    rv.fees={}
    for fee in fees:
        rv.fees[fee.text.split('$')[0]] = fee.text.split('$')[1]
        if 'mileage' in fee.text:
            keyDetails['mileage fee']=fee.text.split('$')[1]
        if 'generator' in fee.text:
            keyDetails['generator fee']=fee.text.split('$')[1]

    nights = detailsoup.find(class_= 'mb2 mt1')
    keyDetails['min nights']=rv.min_nights = nights.text.split(': ')[1]

    scripts = detailsoup.findAll('script')
    idx = 0
    for script in scripts:
        if 'availability' in script.text:
            print('available!', idx, script)
            break
        idx +=1

    detailsExplorer = scripts[idx].text
    if 'availability' in scripts[12].text:
        detailsExplorer = scripts[12].text
        ridFirst = re.sub('<!--', '', detailsExplorer)
        dicotext = re.sub('-->', '', ridFirst)
        detailDico = json.loads(dicotext)
        keyDetails['booked'] = rv.booked = detailDico['availability']['bookedRanges']
    else:
        print('Missed availability on', rv.listing_name)


    toCsv.append(keyDetails)

    print(rv.__dict__)
    print('*** I finished ', link, ' ***')

towrite = pd.DataFrame(toCsv)
towrite.to_csv('RVSharecomps9.28.csv')
