class baseRv:
    '''Class for RV details either way.'''
    listing_name: str
    rv_year: int
    sleeps: int
    location: str
    length: float
    detail_url: str
    id: str

    rates: {}
    fees: {}

    def __init__(self, listing_name):
        self.listing_name = listing_name
