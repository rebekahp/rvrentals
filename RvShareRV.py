from baseRv import baseRv

class RvShareRV(baseRv):
    '''Class for RV details from rvshare.com.'''
    manufacturer: str
    make: str
    model: str
    rv_class: str
    num_beds: str
    fresh_water_tank: str
    height: float
    transmission: str
    cruise_control: str
    engine: str
    seatbelts: int
    fuel_type: str
    fuel_capacity: str
    fuel_mpg: float
    electric_service: str
    dual_battery: bool
    power_steering: bool
    gross_weight: float
    dry_weight: float
    cargo_weight: float
    min_nights: str
    booked: {}
    other_amenities:[]
    rules: []
    id: str
